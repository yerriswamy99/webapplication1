﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace cmnmodels
{
    public class dbmodels
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public string Address { get; set; }
        public string Mobileno { get; set; }
        public string Gmail { get; set; }
    }
}
