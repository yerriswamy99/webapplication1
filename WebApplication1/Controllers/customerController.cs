﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using WebApplication1.Models;
using Businesslayer;
using cmnmodels;


namespace WebApplication1.Controllers
{
    public class customerController : Controller
    {
        businfo info = new businfo();
        // GET: customer
        public ActionResult Index()
        {
            custmodel custmodel = new custmodel();
            custmodel.Id = 1;
            custmodel.Name = "sree";
            custmodel.Address = "Bangalore";
            custmodel.Mobileno = "9908028233";
            custmodel.Gmail = "Sreea@gmail.com";
            ViewData["data"] = custmodel;
           
            return View(custmodel);
        }
        public ActionResult employee()
        {
            ViewData["system"] = "businfo";
            DataSet ds = info.information();
            IList<custmodel> lst = new List<custmodel>();

            //foreach(DataRow dr in ds.Tables[0].Rows)
            //{
            //    custmodel cur = new custmodel();
            //    cur.Id = Convert.ToInt32(dr[0]);
            //    cur.Name = Convert.ToString(dr[1]);
            //    cur.Address = Convert.ToString(dr[2]);
            //    cur.Mobileno = Convert.ToString(dr[3]);
            //    cur.Gmail = Convert.ToString(dr[4]);
            //    lst.Add(cur);
            //}
            lst = (from DataRow dd in ds.Tables[0].Rows
                   select new custmodel()
                   {
                       Id=Convert.ToInt32(dd[0]),
                       Name=Convert.ToString(dd[1]),
                       Address=Convert.ToString(dd[2]),
                       Mobileno=Convert.ToString(dd[3]),
                       Gmail=Convert.ToString(dd[4])

                   }).ToList();
            return View(lst);


        }

        // GET: customer/Details/5
        public ActionResult Details(int id)
        {
            return View();
        }

        // GET: customer/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: customer/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection)
        {
            try
            {
                dbmodels dbn = new dbmodels();
                dbn.Name = collection["Name"];
                dbn.Address = collection["Address"];
                dbn.Mobileno = collection["Mobileno"];
                dbn.Gmail = collection["Gmail"];

                // TODO: Add insert logic here
                info.insertsign(dbn);
                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: customer/Edit/5
        public ActionResult Edit(int id)
        {
            return View();
        }

        // POST: customer/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add update logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }

        // GET: customer/Delete/5
        public ActionResult Delete(int id)
        {
            return View();
        }

        // POST: customer/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            try
            {
                // TODO: Add delete logic here

                return RedirectToAction("Index");
            }
            catch
            {
                return View();
            }
        }
    }
}
